﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador
{
    class ObservadorSeguidor : Observador
    {
        private string nick;
        private SujetoWeb sujetou;

        //Constructor para Seguidor
        public ObservadorSeguidor (string Nick, SujetoWeb NickSujeto)
        {
            nick = Nick;
            sujetou = NickSujeto;
            //
        }

        //Metodo para actualizar sobre cambios de estado
        public void Actualiza(string mensaje)
        {
            Console.WriteLine("Notificar a: {0}'- {1}", nick, mensaje);
        }



    }
}
