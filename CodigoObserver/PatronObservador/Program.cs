﻿using System;

namespace PatronObservador
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creamos la estancia del Sujeto Web, o sea la pagina web que proporciona los videos
            SujetoWeb Web = new SujetoWeb();

            //Se crea las instancias del Observador Seguidor, o sea los seguidores y se agregan a la Pagina Web
            ObservadorSeguidor PrimerSeguidor = new ObservadorSeguidor("TheGamer777", Web);
            Web.agrega(PrimerSeguidor);
            ObservadorSeguidor SegundoSeguidor = new ObservadorSeguidor("PepeVlogs", Web);
            Web.agrega(SegundoSeguidor);

            //Realizamos un ciclo par apoder cambiar de estado del observable
            for (int n = 0; n < 2; n++)
            {
                Web.CambioDeEstado();
            }
            Console.WriteLine();
            //Eliminamos al SegundoSeguidor
            Web.elimina(SegundoSeguidor);

            //Cambiamos y verificamos el estado del observable para que no se notifique el eliminado
            for (int n = 0; n < 2; n++)
            {
                Web.CambioDeEstado();
            }

            Console.WriteLine();
            //Se añade un nuevo seguidor
            ObservadorSeguidor TercerSeguidor = new ObservadorSeguidor("JuniiMC", Web);
            Web.agrega(TercerSeguidor);

            //Nuevamente se hace el cambio de estado del observable y se verifica
            Web.CambioDeEstado();
        }
    }
}
