using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador
{
    class SujetoWeb : Sujeto

    {
        //Se crea una lista que almacenara los observadores que se agreguen al sujeto observador
        private List<Observador> seguidores = new List<Observador>();
        private string mensaje;

        //Se define las funcionalidades de los metodos que implementa el sujeto
        public void agrega(Observador seguidor)
        {
            Console.WriteLine("Se agrego un nuevo seguidor a tu Perfil");
            seguidores.Add(seguidor);
        }
        public void notifica() 
        {
            //Se le notifica el cambio de estado de la pagina a cada seguidor
            foreach (Observador observador in seguidores) 
            {
                observador.Actualiza(mensaje);
            }
        }
         public void elimina(Observador seguidor)
        {
            Console.WriteLine("Se ha eliminado un seguidor de la cuenta");
            seguidores.Remove(seguidor);
        }

        //Metodo usado para realizar un cambio de estado qye se notifica al seguidor
        public void CambioDeEstado() 
        {
            Console.WriteLine("Se añade un nuevo video al perfil de PAXO");
            mensaje = "Se añadió un nuevo video al perfil de PAXO, entra y disfruta";
            notifica();

        }
    }
}
