﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador
{
    //Se crea la interface para poder agregar el estado que actualiza a los seguidores
    interface Observador
    {
        //Esta permite recibir la notificacion del cambio de estado
        void Actualiza(String notificacion);
    }
}
