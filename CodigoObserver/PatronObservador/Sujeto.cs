﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronObservador
{
   interface Sujeto
    {
        //  Permitir agregar seguidores al objeto observado
        void agrega(Observador seguidor);

        //  Elimina al seguidor de la lista del objeto
        void elimina(Observador seguidor);

        // Notifica los cambios de estado
        void notifica();
    }
}
